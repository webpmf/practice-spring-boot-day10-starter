package com.afs.restapi.mapper;

import com.afs.restapi.dto.CompanyCreateRequest;
import com.afs.restapi.dto.CompanyResponse;
import com.afs.restapi.dto.CompanyUpdateRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

public class CompanyMapper {
    public static List<CompanyResponse> getCompanyResponses(List<Company> companies) {
        return companies.stream()
                .map(company -> {
                    CompanyResponse companyResponse = new CompanyResponse();
                    companyResponse.setEmployeeNumbers(company.getEmployees().size());
                    BeanUtils.copyProperties(company, companyResponse);
                    return companyResponse;
                })
                .collect(Collectors.toList());
    }

    public static Company createCompany(CompanyCreateRequest companyCreateRequest) {
        Company company = new Company();
        BeanUtils.copyProperties(companyCreateRequest,company);
        return company;
    }

    public static void updateCompany(CompanyResponse updatingCompanyResponse, Company company) {
        if (updatingCompanyResponse.getName() != null) {
            company.setName(updatingCompanyResponse.getName());
        }
        BeanUtils.copyProperties(updatingCompanyResponse, company);
    }

    public static CompanyResponse getUpdateCompanyResponse(CompanyUpdateRequest companyUpdateRequest, Company company) {
        BeanUtils.copyProperties(companyUpdateRequest, company);
        CompanyResponse companyResponse = new CompanyResponse();
        BeanUtils.copyProperties(company,companyResponse);
        companyResponse.setEmployeeNumbers(company.getEmployees().size());
        return companyResponse;
    }

    public static List<CompanyResponse> getCompanyResponsesByPage(List<Company> companies) {
        return companies.stream()
                .map(company -> {
                    CompanyResponse companyResponse = new CompanyResponse();
                    BeanUtils.copyProperties(company, companyResponse);
                    companyResponse.setEmployeeNumbers(company.getEmployees().size());
                    return companyResponse;
                })
                .collect(Collectors.toList());
    }

    public static CompanyResponse getCompanyResponseById(Company company) {
        CompanyResponse companyResponse = new CompanyResponse();
        BeanUtils.copyProperties(company,companyResponse);
        companyResponse.setEmployeeNumbers(company.getEmployees().size());
        return companyResponse;
    }

    public static CompanyResponse getCompanyResponseCreate(Company company) {
        CompanyResponse companyResponse = new CompanyResponse();
        BeanUtils.copyProperties(company,companyResponse);
        companyResponse.setEmployeeNumbers(company.getEmployees().size());
        return companyResponse;
    }

    public static List<EmployeeResponse> getEmployeeResponses(List<Employee> employees) {
        return employees.stream()
                .map(employee -> {
                    EmployeeResponse employeeResponse = new EmployeeResponse();
                    BeanUtils.copyProperties(employee, employeeResponse);
                    return employeeResponse;
                })
                .collect(Collectors.toList());
    }
}
