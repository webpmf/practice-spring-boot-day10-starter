package com.afs.restapi.mapper;

import com.afs.restapi.dto.EmployeeCreateRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.dto.EmployeeUpdateRequest;
import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EmployeeMapper {
    public static Employee toEntity(EmployeeCreateRequest employeeCreateRequest) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeCreateRequest,employee);
        return employee;
    }

    public static List<EmployeeResponse> getAllEmployeeResponses(List<Employee> employees) {
        return employees.stream()
                .map(employee -> {
                    EmployeeResponse employeeResponse = new EmployeeResponse();
                    BeanUtils.copyProperties(employee,employeeResponse);
                    return employeeResponse;
                })
                .collect(Collectors.toList());
    }

    public static EmployeeResponse getUpdateEmployeeResponse(EmployeeUpdateRequest toUpdate, Employee employee) {
        if (toUpdate.getAge() != null) {
            employee.setAge(toUpdate.getAge());
        }
        if (toUpdate.getSalary() != null) {
            employee.setSalary(toUpdate.getSalary());
        }
        EmployeeResponse employeeResponse = new EmployeeResponse();
        BeanUtils.copyProperties(employee, employeeResponse);
        return employeeResponse;
    }

    public static EmployeeResponse getEmployeeResponseById(Employee employee) {
        EmployeeResponse employeeResponse = new EmployeeResponse();
        BeanUtils.copyProperties(employee, employeeResponse);
        return employeeResponse;
    }

    public static List<EmployeeResponse> getEmployeeResponsesByGender(List<Employee> employees) {
        return employees.stream()
                .map(employee -> {
                    EmployeeResponse employeeResponse = new EmployeeResponse();
                    BeanUtils.copyProperties(employee, employeeResponse);
                    return employeeResponse;
                })
                .collect(Collectors.toList());
    }

    public static List<EmployeeResponse> getEmployeeResponsesByPage(List<Employee> employees) {
        return employees.stream()
                .map(employee -> {
                    EmployeeResponse employeeResponse = new EmployeeResponse();
                    BeanUtils.copyProperties(employee, employeeResponse);
                    return employeeResponse;
                })
                .collect(Collectors.toList());
    }

    public static EmployeeResponse getEmployeeResponseInsert(Employee employee) {
        EmployeeResponse employeeResponse = new EmployeeResponse();
        BeanUtils.copyProperties(employee,employeeResponse);
        return employeeResponse;
    }
}
