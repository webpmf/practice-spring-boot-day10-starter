package com.afs.restapi.service;

import com.afs.restapi.dto.CompanyCreateRequest;
import com.afs.restapi.dto.CompanyResponse;
import com.afs.restapi.dto.CompanyUpdateRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.mapper.CompanyMapper;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {
    private final CompanyRepository companyRepository;
    private final EmployeeRepository employeeRepository;

    public CompanyService(CompanyRepository companyRepository, EmployeeRepository employeeRepository) {
        this.companyRepository = companyRepository;
        this.employeeRepository = employeeRepository;
    }

    public List<CompanyResponse> getAll() {
        return CompanyMapper.getCompanyResponses(companyRepository.findAll());
    }

    public List<CompanyResponse> getAll(Integer page, Integer pageSize) {
        List<Company> companies = companyRepository.findAll(PageRequest.of(page, pageSize)).toList();
        return CompanyMapper.getCompanyResponsesByPage(companies);
    }

    public CompanyResponse findById(Integer companyId) {
        Company company = companyRepository.findById(companyId).orElseThrow(CompanyNotFoundException::new);
        return CompanyMapper.getCompanyResponseById(company);
    }

    public CompanyResponse create(CompanyCreateRequest companyCreateRequest) {
        Company company = companyRepository.save(CompanyMapper.createCompany(companyCreateRequest));
        CompanyResponse companyResponse = CompanyMapper.getCompanyResponseCreate(company);
        return companyResponse;
    }

    public void deleteCompany(Integer companyId) {
        Optional<Company> company = companyRepository.findById(companyId);
        company.ifPresent(companyRepository::delete);
    }

    public CompanyResponse update(Integer companyId, CompanyUpdateRequest companyUpdateRequest) {
        Company company = companyRepository.findById(companyId)
                .orElseThrow(CompanyNotFoundException::new);
        CompanyResponse companyResponse = CompanyMapper.getUpdateCompanyResponse(companyUpdateRequest, company);
        companyRepository.save(company);
        return companyResponse;
    }

    public List<EmployeeResponse> getEmployees(Integer companyId) {
        List<Employee> employees = companyRepository.findById(companyId)
                .orElseThrow(CompanyNotFoundException::new)
                .getEmployees();
        return CompanyMapper.getEmployeeResponses(employees);
    }

}
