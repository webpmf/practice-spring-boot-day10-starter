package com.afs.restapi.service;

import com.afs.restapi.dto.EmployeeCreateRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.dto.EmployeeUpdateRequest;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<EmployeeResponse> findAll() {
        return EmployeeMapper.getAllEmployeeResponses(employeeRepository.findAllByStatusTrue());
    }

    public EmployeeResponse update(int id, EmployeeUpdateRequest toUpdate) {
        Employee employee = employeeRepository.findByIdAndStatusTrue(id)
                .orElseThrow(EmployeeNotFoundException::new);
        EmployeeResponse employeeResponse = EmployeeMapper.getUpdateEmployeeResponse(toUpdate, employee);
        employeeRepository.save(employee);
        return employeeResponse;
    }

    public EmployeeResponse findById(int id) {
        Employee employee = employeeRepository.findByIdAndStatusTrue(id)
                .orElseThrow(EmployeeNotFoundException::new);
        return EmployeeMapper.getEmployeeResponseById(employee);
    }

    public List<EmployeeResponse> findByGender(String gender) {
        return EmployeeMapper.getEmployeeResponsesByGender(employeeRepository.findByGenderAndStatusTrue(gender));
    }

    public List<EmployeeResponse> findByPage(int pageNumber, int pageSize) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);
        return EmployeeMapper.getEmployeeResponsesByPage(employeeRepository.findAllByStatusTrue(pageRequest).toList());
    }

    public EmployeeResponse insert(EmployeeCreateRequest employeeCreateRequest) {
        return EmployeeMapper.getEmployeeResponseInsert(employeeRepository.
                save(EmployeeMapper.toEntity(employeeCreateRequest)));
    }

    public void delete(int id) {
        Employee employee = employeeRepository.findByIdAndStatusTrue(id)
                .orElseThrow(EmployeeNotFoundException::new);
        employee.setStatus(false);
        employeeRepository.save(employee);
    }
}
