package com.afs.restapi.dto;

public class EmployeeUpdateRequest {
    private Integer salary;
    private Integer age;

    public EmployeeUpdateRequest() {
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }
}
