package com.afs.restapi.dto;

public class CompanyUpdateRequest {
    private String name;

    public CompanyUpdateRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
