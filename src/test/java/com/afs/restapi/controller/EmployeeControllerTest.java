package com.afs.restapi.controller;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class EmployeeControllerTest {

    // GET "/employees"
    @Autowired
    MockMvc client;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    CompanyRepository companyRepository;

    private Company preparedCompany;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void prepareData() {
        employeeRepository.deleteAll();
        Company company = new Company();
        company.setName("afs");
        preparedCompany = companyRepository.save(company);
    }

    @Test
    void should_get_all_employees_when_perform_get_given_employees() throws Exception {
        // given
        employeeRepository.save(givenEmploySusan());

        // when
        client.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Susan"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(22))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value("Female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].companyId").value(preparedCompany.getId()));
        // should
    }

    @Test
    void should_create_new_employee_when_perform_post_given_new_employee() throws Exception {
        // given

        String newEmployeeJson = "{"+
                "\"name\":\"Susan\"," +
                "\"age\":22," +
                "\"gender\":\"Female\"," +
                "\"salary\":10000," +
                "\"companyId\":1," +
                "\"status\":true}";
        // when
        client.perform(MockMvcRequestBuilders.post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(newEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Susan"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(22))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("Female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.companyId").value(1));

        // should
        List<Employee> employees = employeeRepository.findAll();
        assertThat(employees, hasSize(1));
        assertThat(employees.get(0).getName(), equalTo("Susan"));
        assertThat(employees.get(0).getAge(), equalTo(22));
        assertThat(employees.get(0).getGender(), equalTo("Female"));
    }

    // Demo the above 2 tests

    @Test
    void should_return_employees_when_perform_get_by_page_given_employees() throws Exception {
        // given
        employeeRepository.save(new Employee(1, "Susan", 25, "Female", 9000, preparedCompany.getId(), true));
        employeeRepository.save(new Employee(2, "Lisa", 22, "Female", 10000, preparedCompany.getId(), true));

        // when
        client.perform(MockMvcRequestBuilders.get("/employees")
                        .param("page", "0")
                        .param("size", "2")
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].name", containsInAnyOrder("Susan", "Lisa")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].age", containsInAnyOrder(22, 25)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].gender", containsInAnyOrder("Female", "Female")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].companyId", containsInAnyOrder(preparedCompany.getId(), preparedCompany.getId())));

        // should
    }

    @Test
    void should_return_employee_by_id_when_perform_get_given_employee() throws Exception {
        // given
        Employee inserted = employeeRepository.save(
                new Employee(1, "Susan", 25, "Female", 9000, preparedCompany.getId(), true)
        );

        // when
        client.perform(MockMvcRequestBuilders.get("/employees/{id}", inserted.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Susan"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(25))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("Female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.companyId").value(preparedCompany.getId()));

        // should
    }

    @Test
    void should_return_404_when_perform_get_by_id_given_id_not_exist() throws Exception {
        // given

        // when
        client.perform(MockMvcRequestBuilders.get("/employees/999"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        // should
    }

    @Test
    void should_return_updated_employee_when_perform_put_given_employee() throws Exception {
        // given
        Employee employee = employeeRepository.save(givenEmploySusan());

        // when
        client.perform(MockMvcRequestBuilders.put("/employees/{id}", employee.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "    \"name\": \"Lisa\",\n" +
                                "    \"age\": 30,\n" +
                                "    \"gender\": \"Male\",\n" +
                                "    \"salary\": 90000\n" +
                                "}"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Susan"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(30))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("Female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.companyId").value(preparedCompany.getId()));

        // should
        Employee updatedEmployee = employeeRepository.findAll().get(0);
        assertThat(updatedEmployee.getId(), equalTo(employee.getId()));
        assertThat(updatedEmployee.getName(), equalTo("Susan"));
        assertThat(updatedEmployee.getAge(), equalTo(30));
        assertThat(updatedEmployee.getGender(), equalTo("Female"));
    }

    @Test
    void should_return_404_when_perform_put_by_id_given_id_not_exist() throws Exception {
        // given

        // when
        client.perform(MockMvcRequestBuilders.put("/employees/999")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "    \"name\": \"Lisa\",\n" +
                                "    \"age\": 30,\n" +
                                "    \"gender\": \"Male\",\n" +
                                "    \"salary\": 90000\n" +
                                "}"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        // should
    }

    @Test
    void should_return_204_when_perform_delete_given_employee() throws Exception {
        // given
        Employee insertedEmployee = employeeRepository.save(givenEmploySusan());

        // when
        client.perform(MockMvcRequestBuilders.delete("/employees/{id}", insertedEmployee.getId()))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        // should
        assertThat(employeeRepository.findAllByStatusTrue(), empty());
    }

    @Test
    void should_return_404_when_perform_delete_by_id_given_id_not_exist() throws Exception {
        // given

        // when
        client.perform(MockMvcRequestBuilders.delete("/employees/999"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        // should
    }

    private Employee givenEmploySusan() {
        return new Employee(1, "Susan", 22, "Female", 10000, preparedCompany.getId(), true);
    }
}
